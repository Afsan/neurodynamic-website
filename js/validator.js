//-------------------validator for contact us-----------//
function validation()
{
var username =document.getElementById('Name').value;
var email = document.getElementById('Email').value;
var subject =document.getElementById('Subject').value;
var comment =document.getElementById('Comment').value;

var usercheck = /^[A-Za-z. ]{3,30}$/
var lastnamecheck = /^[A-Za-z. ]{3,30}$/ 
var emailcheck = /^[A-Za-z_]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/

if(usercheck.test(username)){
    document.getElementById('usererror').innerHTML = " "
}
else
{
    document.getElementById('usererror').innerHTML = "** Username is Invalid"
    return false;
}
if(lastnamecheck.test(lastname)){
    document.getElementById('lasterror').innerHTML = " "
}
else
{
    document.getElementById('lasterror').innerHTML = "** Username is Invalid"
    return false;
}

if(emailcheck.test(email)){
    document.getElementById('emailerror').innerHTML = " "
}
else
{
    document.getElementById('emailerror').innerHTML = "** Email is Invalid"
    return false;
}
}

//------------------------------------validator for career page-----------------------------------------//


function validationform()
{
    var FirstName = document.getElementById('FName').value;
    var LasttName = document.getElementById('LName').value;
    var phone = document.getElementById('Phonenumber').value;
    var email = document.getElementById('Emails').value;
    var Company = document.getElementById('Company').value;

    var usercheck = /^[A-Za-z. ]{3,30}$/
    var phonecheck = /\+?\d[\d -]{8,10}\d/
    var Companycheck=/[a-zA-Z\d.*\/]{2,20}/
    var emailcheck = /^[A-Za-z_]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/

    if(usercheck.test(FirstName)){
        document.getElementById('usererror').innerHTML = " "
    }
    else
    {
        document.getElementById('usererror').innerHTML = "** First Name is Invalid"
        return false;
    }
    if(usercheck.test(LasttName)){
        document.getElementById('Lasterror').innerHTML = " "
    }
    else
    {
        document.getElementById('Lasterror').innerHTML = "** Last Name is Invalid"
        return false;
    }
    if(phonecheck.test(phone)){
        document.getElementById('phoneerror').innerHTML = " "
    }
    else
    {
        document.getElementById('phoneerror').innerHTML = "** Phone Number is Invalid"
        return false;
    }
    if(emailcheck.test(email)){
        document.getElementById('emailerror').innerHTML = " "
    }
    else
    {
        document.getElementById('emailerror').innerHTML = "** Email is Invalid"
        return false;
    }
    if(Companycheck.test(Company)){
        document.getElementById('Companyerror').innerHTML = " "
    }
    else
    {
        document.getElementById('Companyerror').innerHTML = "** Company Name is Invalid"
        return false;
    }
}