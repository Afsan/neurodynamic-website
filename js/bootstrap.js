function validationform()
{
    var FirstName = document.getElementById('FName').value;
    var LasttName = document.getElementById('LName').value;
    var email = document.getElementById('Emails').value;
    var Company = document.getElementById('Company').value;

    var usercheck = /^[A-Za-z. ]{3,30}$/
    var emailcheck = /^[A-Za-z_]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/

    if(usercheck.test(FirstName)){
        document.getElementById('usererror').innerHTML = " "
    }
    else
    {
        document.getElementById('usererror').innerHTML = "** First Name is Invalid"
        return false;
    }
    if(usercheck.test(LasttName)){
        document.getElementById('Lasterror').innerHTML = " "
    }
    else
    {
        document.getElementById('Lasterror').innerHTML = "** Last Name is Invalid"
        return false;
    }
    if(emailcheck.test(email)){
        document.getElementById('emailerror').innerHTML = " "
    }
    else
    {
        document.getElementById('emailerror').innerHTML = "** Email is Invalid"
        return false;
    }
    if(usercheck.test(Company)){
        document.getElementById('Companyerror').innerHTML = " "
    }
    else
    {
        document.getElementById('Companyerror').innerHTML = "** Company Name is Invalid"
        return false;
    }
}